use awesome_lib::add;
use awesome_lib2::square;
fn main() {
    println!("Hello, world!");
    println!("packages/awesome_lib add function result {}", add(2, 3));
    println!("packages/awesome_lib square function result {}", square(2));
}
