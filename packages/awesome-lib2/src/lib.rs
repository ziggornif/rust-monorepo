pub fn square(num: i32) -> i32 {
    num * num
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = square(2);
        assert_eq!(result, 4);
    }
}
